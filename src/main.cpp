#include <Arduino.h>
#include <Adafruit_Sensor.h>
#include <DHT.h>
#include <DHT_U.h>
#include <WiFi.h>
#include <HTTPClient.h>
#include <FirebaseESP32.h>

#define DHTPIN 16
#define DHTTYPE DHT22
#define RELAYPIN 2
#define RELAYPIN2 17
#define FIREBASE_HOST "https://hastasm-e4799.firebaseio.com/"
#define FIREBASE_AUTH "RWyALJ3DvSh7nZbU79zktL6blgda5JQqBzs3T0fQ"

//DHT_Unified dht(DHTPIN, DHTTYPE);
DHT dht(DHTPIN, DHTTYPE);
uint32_t delayMs = 10000;

FirebaseData firebaseData;

const char *ssid = "S.TS..2.4GHz";
const char *pass = "Bongaon:743235";

String host = "http://192.168.0.102:9999/device/d1/temperature/28.Celcius";

float temp;
float humid;

unsigned long old_time;

void setupWiFiAsAccessPoint() {
  WiFi.begin(ssid, pass);

  while(WiFi.status() != WL_CONNECTED) {
    Serial.print(".");
    delay(500);
  }

  Serial.println("");
  Serial.println("WiFi connected....");
  Serial.println(WiFi.localIP());

  Firebase.begin(FIREBASE_HOST, FIREBASE_AUTH);

}

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  old_time = 0;
  setupWiFiAsAccessPoint();
  dht.begin();
  /**
  Serial.println("DHT Sensor Data");
  sensor_t sensor;
  dht.temperature().getSensor(&sensor);
  Serial.println("---------------------------------");
  Serial.println("Temperature");
  Serial.print("Max value:    ");
  Serial.print(sensor.max_value);
  Serial.println(" *C");
  delayMs = 10000;
  **/
  //pinMode(17, OUTPUT);
  pinMode(RELAYPIN, OUTPUT);
  pinMode(RELAYPIN2, OUTPUT);
}

void sendTemperatureToServer(float temp) {
  if (WiFi.status() == WL_CONNECTED) {
    HTTPClient http;
    //String url = "http://192.168.0.102:9999/device/d1/settemperature/" + String(temp) + ".Celcius";
    http.begin("192.168.0.102", 9999, "/device/d1/settemperature/" + String(temp) + ".Celcius");
    int httpCode = http.GET();
    Serial.print("httpCode: ");
    Serial.println(httpCode);
    http.end();

    delay(1000);
  }
}

void sendHumidityToServer(float humidity) {
  if (WiFi.status() == WL_CONNECTED) {
    HTTPClient http;
    //String url = "http://192.168.0.102:9999/device/d1/settemperature/" + String(temp) + ".Celcius";
    http.begin("192.168.0.102", 9999, "/device/d1/sethumidity/" + String(humidity) + ".percent");
    int httpCode = http.GET();
    Serial.print("httpCode: ");
    Serial.println(httpCode);
    http.end();

    delay(1000);
  }
}

void loop() {
  // put your main code here, to run repeatedly:
  temp = dht.readTemperature();
  humid = dht.readHumidity();
  if (!isnan(temp) && !isnan(humid) && (millis() - old_time) > 1000) {
    Serial.print("Temperature: ");
    Serial.println(temp);
    Serial.print("Humidity: ");
    Serial.println(humid);

      if ((millis() - old_time) > 30000) {
        sendTemperatureToServer(temp);
        sendHumidityToServer(humid);
        old_time = millis();
      }
  } else {
    Serial.println("Please connect DHT module...");
    delay(1000);
  }

  if (Firebase.getInt(firebaseData, "/light")) {
    if (firebaseData.dataType() == "int") {
      int val = firebaseData.intData();
      if (val == 0) {
        Serial.println("Light Off");
        digitalWrite(RELAYPIN, LOW);
      } else {
        Serial.println("Light On");
        digitalWrite(RELAYPIN, HIGH);
      }
    }
  }

  if (Firebase.getInt(firebaseData, "/fan")) {
    if (firebaseData.dataType() == "int") {
      int val = firebaseData.intData();
      if (val == 0) {
        Serial.println("Fan Off");
        digitalWrite(RELAYPIN2, LOW);
      } else {
        Serial.println("Fan On");
        digitalWrite(RELAYPIN2, HIGH);
      }
    }
  }

  delay(1000);
  /**
  delay(5000);
  digitalWrite(17, HIGH);
  Serial.println("LED is ON..");
  delay(500);
  digitalWrite(17, LOW);
  Serial.println("LED is OFF..");
  delay(500);
  **/
}
